\documentclass{beamer}

\usepackage[T1]{fontenc}
\usepackage{fontspec}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{graphicx}


\def\model#1#2{{#1}{\scriptsize2}{#2}} % from https://arxiv.org/abs/1703.07684 tex source (for writing S2S etc.)


\usetheme{metropolis}
\title{Predicting Deeper into the Future of Semantic Segmentation (Neverova, Luc 2017)}
\date{May 30, 2017}
\author{Martin Drawitsch}

\begin{document}

\maketitle

\begin{frame}{Content}
    \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Problem: Predicting future video frames}
    \begin{itemize}
        \item Predicting future video frames of street scenes \\
        $\rightarrow$ Autonomous intelligent decisions
        %\item "How will this scene look in x seconds?"
        \item Provide minimal input for decision making
        \begin{itemize}
            \item Self-driving cars
            \item Robotics
        \end{itemize}
    \end{itemize}
    \begin{figure}
        \includegraphics[width=5cm]{img/selfdrivingcar.jpg}
        % image source: http://shyrobotics.com/2012/05/14/la-premiere-voiture-sans-pilote-immatriculee-est-une-google-car-cest-au-nevada/
    \end{figure}
\end{frame}

\begin{frame}{Ideas}
    \begin{itemize}
        \item No need to predict future RGB video frames for decisions
        \item Don't even require previous RGB video frames as input
        \item Directly predict future frame segmentations from given preceding frame segmentations
    \end{itemize}
    \vspace{-4mm}
    \begin{figure}
        \includegraphics[width=5cm]{img/overview.pdf}
        \caption{Semantic segmentation of a street scene (overlayed on RGB
                 frames, with expected directions of movement)}
    \end{figure}
\end{frame}

\begin{frame}{Goals and non-goals}
    \begin{itemize}
        \item Goals
        \begin{itemize}
            \item Convincing prediction of future semantic segmentations
            \item Focus on short-term future ($\leq$ 0.5s)
            \item Automatic segmentation of input video
                  (no need for expensive manually labeled GT)
            \item Proposed pipeline: RGB video $\rightarrow$ dense video segmentation $\rightarrow$ future segmentation prediction
        \end{itemize}
        \item Non-goals
        \begin{itemize}
            \item RGB video predictions
            \item Actual decision making system
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Previous work}
    \begin{itemize}
        \item Warping segmentations using optical flow between preceding frames
              (Patraucean 2015 {\tiny\url{https://arxiv.org/abs/1511.06309}})
        %\item Generative LSTM (Long Short Term Memory) network architectures
        %      (Srivastava 2015 {\tiny\url{https://arxiv.org/abs/1502.04681}})
        \item Multi-scale CNN with adversarial and gradient difference loss
              (Mathieu 2015 {\tiny\url{https://arxiv.org/abs/1511.05440}})
              %(used in this paper, see section "Future frame prediction")
        \item Using GANs (Generative Adversarial Networks) to convincingly predict future video frames
              (Vondrick 2016 {\tiny\url{https://arxiv.org/abs/1609.02612}})
    \end{itemize}
\end{frame}

\section{Models I: Temporally dense video segmentation}

\begin{frame}{Obtaining semantic frame segmentations}
    \begin{itemize}
        \item Training a network to predict future segmentation maps
              requires video frame segmentations
        \item Don't use manually labelled data but generate it automatically
        \item Re-use the existing Dilation10 network for this task
    \end{itemize}
    \begin{figure}
        \includegraphics[width=11cm]{img/dilationpaper_cityscapes_rgb-gtlab.png}
        \caption{RGB video frame $\rightarrow$ semantic segmentation (GT)}
    \end{figure}
\end{frame}

\begin{frame}{Dilation10 network (Yu, Koltun 2015)}
    \begin{itemize}
        \item Modified VGG-16 architecture with
              multi-scale context aggregation
        \item Context aggregation based on dilated convolution
    \end{itemize}
    \begin{figure}
        \includegraphics[width=6cm]{img/vgg-16.png}
        \caption{Original VGG-16 network (Simonyan, Zisserman 2014)}
    \end{figure}
\end{frame}

\begin{frame}{Dilated convolution}
    \begin{itemize}
        \item Regular discrete convolution ($\ast$)
            \begin{equation}
                (F \ast k)(p) = \sum_{s + t = p} F(s) \, k(t).
            \end{equation}
        \item Dilated convolution ($\ast_d$)
            \begin{equation} \label{dilconv}
                (F \ast_d k)(p) = \sum_{s + d t = p} F(s) \, k(t).
            \end{equation}
        %\item Expanding RF (receptive field) while preserving resolution and coverage
    \end{itemize}
\end{frame}

\begin{frame}{2D Dilated convolution (visualisation)}
    \begin{equation}
        (F \ast_d k)(p) = \sum_{s + d t = p} F(s) \, k(t).
        \tag{2}
    \end{equation}
    \begin{figure}
        \includegraphics[width=8cm]{img/dilconvvis.png}
        \caption{2D Dilated convolution $(F \ast_d k)$ with:
                 \textbf{(a)} $d = 1$ $\rightarrow$ RF $3\times3$.
                 \textbf{(b)} $d = 2$ on output of (a) $\rightarrow$ RF $7\times7$.
                 \textbf{(c)} $d = 4$ on output of (b) $\rightarrow$ RF $15\times15$.}
    \end{figure}
    \vspace{-5mm}
    $\rightarrow$ Expanding RF (receptive field) while preserving resolution and coverage
\end{frame}

\begin{frame}{Context aggregation layers}
    \begin{table} % based on table 1 from https://arxiv.org/abs/1511.07122
        \centering
        \small
        \begin{tabular}{lccccc}
            Layer                 & 1 & 2 & 3 & 4 & 5 \\
            \midrule
            Convolution kernel    & $3\times 3$ & $3\times 3$ & $3\times 3$ & $3\times 3$ & $3\times 3$\\ \hline
            RF (regular conv)     & $3\times 3$ & $5\times 5$ & $7\times 7$ & $9\times 9$ & $11\times 11$\\ \hline
            Dilation factor ($d$) & 1 & 1 & 2 & 4 & 8\\ \hline
            RF (dilated conv)     & $3\times 3$ & $5\times 5$ & $9\times 9$ & $17\times 17$ & $33\times 33$\\
        \end{tabular}
        \caption{Effect of dilated convolutions on the receptive field, compared with regular convolutions.}
    \end{table}
\end{frame}


\section{Models II: Future segmentation prediction}

\begin{frame}{Problems with normal CNNs for next-frame prediction}
    \begin{figure}
        \includegraphics[width=10cm]{img/naive_convnet__predbeyondmse25.png}
        \caption{CNN ($G$) trained to predict future frame Y from concatened preceding frames X,
                 using loss: $\mathcal{L}_p(X,Y) = \|G(X) - Y\|_p^p, p = 1, 2$.}
    \end{figure}
    \vspace{-4mm}
    Problems:
    \begin{itemize}
        \item No pooling: RF too small to be useful, adequate RF would require too many layers
        \item Pooling: Reduces output size $\rightarrow$ unsuitable
        \item Predictions are blurry ($\rightarrow$ loss?)
    \end{itemize}
\end{frame}

\begin{frame}{Solution: Multi-scale video prediction model (Mathieu 2015)}
    \begin{figure}
        \includegraphics[width=10.5cm]{img/multiscale_nextframe__predbeyondmse25.png}
        \caption{Section of proposed multi-scale architecture}
    \end{figure}
    \vspace{-4mm}
    Solution for RF/pooling problems:
    \begin{itemize}
        \item Compute $G_k(X)$ with different input/output sizes $s_k$
        \item Use upsampled $G_k(X)$ as additional input for $G_{k+1}(X)$
        \item Mathieu: $G = G_4$; $s_1 = 4\times4$, ... $s_4 = 32\times32$
        \item Implementation in NevLuc2017:  $G = G_2$; $s_1 = 64\times128, s_2 = 128\times256$
        % NevLuc2017 numbers from section 4.1, assuming Dilation10 output is directly used as input
    \end{itemize}
\end{frame}

\begin{frame}{Multi-scale video prediction model: details}
    \begin{itemize}
        \item $G_1$ (small scale) architecture:
            \begin{itemize}
                \item $X_1$ $\rightarrow$ conv $\rightarrow$ conv $\rightarrow$
                      conv $\rightarrow$ conv $\rightarrow$ $G_1(X)$
                \item kernel sizes (quadratic): 3, 3, 3, 3
            \end{itemize}
        \item $G_2$ (full scale) architecture:
            \begin{itemize}
                \item $up_2(G_1(X)) + X_2$ $\rightarrow$ conv $\rightarrow$ conv $\rightarrow$
                      conv $\rightarrow$ conv $\rightarrow$ $\hat{Y}$
                \item kernel sizes (quadratic): 5, 3, 3, 5
            \end{itemize}
        \item $n_{features}$ (both $G_1$ and $G_2$): 128, 256, 128, C
    \end{itemize}
    \small where
    \begin{itemize}
        %\item Combining 2 scales ($s_1 = 64\times128, s_2 = 128\times256$)
        \item $X_{1,2}$: Input (concatened preceding frames) at half/full scale
        \item conv: Convolution layer followed by reLU
        \item C: Number of output channels
        \item $up_2$: upsample ($2\times2$)
        \item $+$: concatenate layers
    \end{itemize}
\end{frame}

\begin{frame}{Loss function for next-frame prediction}
    \begin{itemize}
        \item $\hat{Y} \equiv G(X)$ (predicted output)
        \item $Y_{i,j}$/$\hat{Y}_{i,j}$: pixel at location $(i, j)$ in $Y$/$\hat{Y}$
        \item Sum of $\ell_1$ loss and gradient difference loss (gdl)
            \begin{equation}
                \mathcal{L}(\hat{Y},Y) = \mathcal{L}_{\ell_1}(\hat{Y},Y) + \mathcal{L}_{gdl}(\hat{Y},Y)
            \end{equation}
        \item where
            \begin{eqnarray} % mostly copied from https://arxiv.org/abs/1703.07684 tex source
                \mathcal{L}_{\ell_1}(\hat{Y},Y) & = & \sum_{i,j} \big|
                Y_{i,j}-\hat{Y}_{i,j}\big|,\\ \mathcal{L}_{\textrm{gdl}}(\hat{Y},Y) & = &
                \sum_{i,j} \Big| \big|Y_{i,j}-Y_{i-1,j}\big| -
                \big|\hat{Y}_{i,j}-\hat{Y}_{i-1,j}\big| \Big| \nonumber \\ && \hspace{3.5mm} + \Big|
                \big|Y_{i,j-1}-Y_{i,j}\big| - \big|\hat{Y}_{i,j-1}-\hat{Y}_{i,j}\big| \Big|
            \end{eqnarray}
    \end{itemize}
\end{frame}

\begin{frame}{Why $\ell_1$ and gradient difference loss?}
    \begin{itemize} % Paraphrased from Loss section in https://arxiv.org/abs/1703.07684
        \item $\mathcal{L}_{\ell_1}$: $|\hat{Y}_{i,j}$ - $Y_{i,j}|$ for each independent $(i, j)$ $\rightarrow$ blurry results
        \item $\mathcal{L}_{gdl}$: errors in local gradients of $\hat{Y}$, $Y$ \\
              $\rightarrow$ Forgive low-freq. differences \\
              $\rightarrow$ Emphasise high-freq. differences (e.g. blur)
    \end{itemize}
    \begin{figure}
        \includegraphics[width=7cm]{img/gdl-gradients.png}
        \caption{Images A, B ($B = A + 100$): equal gradients, but different absolute pixel values
                 $\rightarrow$ high $\mathcal{L}_{\ell_1}(A, B)$, but $\mathcal{L}_{gdl}(A, B) = 0$ }
    \end{figure}
    \vspace{-3mm}
    $\rightarrow$ More realistic predictions, but still room for improvement...

\end{frame}

\begin{frame}{Adversarial training}
    \begin{itemize}
        \item \textbf{Realistic} predictions for \textbf{decision making}
        \item Penalize fake-looking predictions (blur, atypical shapes, esp. "undecided" outputs that average over possibilities) \\
              $\rightarrow$ Use a (W)GAN for additional loss
        \item Train discriminator $\mathcal{D}_\Theta$:
            \begin{equation}
                \max_{\Theta} \big|
                \sigma(\mathcal{D}_\Theta(Y_{1:t},Y_{t+1})) - \sigma(\mathcal{D}_\Theta(Y_{1:t},\hat{Y}_{t+1}))\big|
            \end{equation}
            where
            \begin{itemize}
                \item $\Theta$: set of trainable parameters of $\mathcal{D}$
                \item $(Y_{1:t}, Y_{t+1})$: GT frame sequence
                \item $(Y_{1:t}, \hat{Y}_{t+1})$ sequence predicted by $G$
                \item $\sigma$: sigmoid nonlinearity to stabilize training
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Models for next-frame prediction}
    \begin{table} % based on table 1 from https://arxiv.org/abs/1703.07684
        \begin{center}
            \begin{tabular}{lcc}
                \toprule
                Model & Input & Output\\
                \midrule
                \model{X}{X}   &  $X_{1:t}$            & $X_{t+1}$ \\
                \model{S}{S}   &  $S_{1:t}$            & $S_{t+1}$ \\
                \model{XS}{X}  &  $(X_{1:t}, S_{1:t})$ & $X_{t+1}$ \\
                \model{XS}{S}  &  $(X_{1:t}, S_{1:t})$ & $S_{t+1}$ \\
                \model{XS}{XS} &  $(X_{1:t}, S_{1:t})$ & $(X_{t+1},S_{t+1})$ \\
                \bottomrule
            \end{tabular}
        \end{center}
        \caption{Different model outlines for next-frame prediction.}
    \end{table}
    where
    \begin{itemize}
        \item X: RGB frame
        \item S: Segmented frame (using Dilation10)
        \item t: time (frame number)
    \end{itemize}
\end{frame}

\begin{frame}{Single-frame predictions}
    \begin{figure}
        \includegraphics[width=7cm]{img/singleframe__fseg.pdf}
        \caption{Single-frame prediction model.}
    \end{figure}
    \begin{itemize}
        \item In: preceding segmented frames $S_{(t-3):t}$
        \item Out: single next-frame segmentation prediction $S_{t+1}$
        \item (Not our focus here - we want multiple future frames)
    \end{itemize}
\end{frame}

\begin{frame}{Multi-frame predictions: Batch model}
    \begin{figure}
        \includegraphics[width=7cm]{img/batch__fseg.pdf}
        \caption{Multi-frame prediction using batch model}
    \end{figure}
    \vspace{-4mm}
    \begin{itemize}
        \item In: preceding segmented frames $S_{(t-3):t}$
        \item Out: m next frames $S_{(t+1):(t+m)}$
        \item Using the same inputs for all m future frames \\
              $\rightarrow$ neglecting dependencies between past frames \\
              (e.g. compare $S_{t-3} \sim S_{t-2}$ with $S_{t+2} \sim S_{t+3}$)
    \end{itemize}
\end{frame}

\begin{frame}{Multi-frame predictions: AR (autoregressive) model}
    \begin{figure}
        \includegraphics[width=7cm]{img/autoreg__fseg.pdf}
        \caption{Multi-frame prediction using AR model.}
    \end{figure}
    \vspace{-4mm}
    \begin{itemize}
        \item Out: m next frames $S_{(t+1):(t+m)}$
        \item In: Different for each $t$: Always use n (4) direct predecessors \\
              $\rightarrow$ Use earlier predicted frames \\
        $\rightarrow$ Arbitrarily long future predictions?
    \end{itemize}
\end{frame}


\section{Experiments and evaluations}

\begin{frame}{Dataset: Cityscapes (2016)}
    \begin{figure}
        \includegraphics[width=10cm]{img/fig10__cityscapes.png}
        \caption{Example image-annotation pair of the cityscape dataset}
    \end{figure}
    \begin{itemize}
        \item Cityscapes video data set for urban scene understanding (2016)
        \item Video sequences: 2975 training, 500 validation, 1525 testing, each 1.8 s (30 frames)
        \item GT segmentation for each clip's 20th frame available
    \end{itemize}
\end{frame}

\begin{frame}{Metrics}
    \begin{figure}
        \includegraphics[width=7cm]{img/detection_iou.png}
        \caption{Visualisation of IoU for bounding box prediction.}
        % image source: https://people.cs.pitt.edu/~kovashka/cs1699/hw4.html
    \end{figure}
    \vspace{-4mm}
    \begin{itemize}
        \item IoU GT: IoU (Intersection over Union) of predicted future segmentation w.r.t. GT segmentation
        \item IoU SEG: IoU of future segmentation prediction w.r.t. Dilation10 segmentation
        \item IoU-MO: Mean IoU of mobile objects (bike, car, \textbf{not} road)
    \end{itemize}
\end{frame}

\begin{frame}{Baselines}
    Two baselines for accuracy comparison:
        \begin{enumerate}
            \item Copy last input frame (just set $S_{t+n} = S_t$) % as if nothing will happen
            \item Warp last input frame with optical flow between last two input frames (using FlowNet (Fischer, 2015))
        \end{enumerate}
\end{frame}

\begin{frame}{Short-term future (1 frame)}
    \begin{itemize}
        \item Inputs: frames 8, 11, 14, 17
        \item Output: frame 20 (Seg-GT available in Cityscapes)
    \end{itemize}
    \begin{table}
        \includegraphics[width=10cm]{img/table2_shortterm_eval__fseg.png}
        \caption{Evaluating single-frame prediction models.}
    \end{table}
\end{frame}

\begin{frame}{Short-term future (1 frame): \model{S}{S} ablation}
    Focus on \model{S}{S} (no adversarial loss):
    \begin{itemize}
        \item Different losses? \\
              (MCE = Multi-class Cross-Entropy loss)
        \item No multi-scale architecture?
    \end{itemize}
    \begin{table}
        \includegraphics[width=9cm]{img/table3_shortterm_s2s_ablation__fseg.png}
        \caption{Ablation: \model{S}{S} model variations.}
    \end{table}
\end{frame}

\begin{frame}{Mid-term future ($\leq$ 0.5 sec)}
    \small{\begin{itemize}
        \item Inputs: frames 2, 5, 8, 11
        \item Outputs: frames 14, 17, 20 ($\sim$ 0.5 sec into future)
    \end{itemize}}
    \vspace{-3mm}
    \begin{table}
        \includegraphics[width=7cm]{img/table5_midterm_eval__fseg.png}
        \caption{Accuracy of segmentation prediction $\sim$ 0.5 sec into future.}
    \end{table}
    % skipping RGB prediction models here
\end{frame}

\begin{frame}{Mid-term future ($\leq$ 0.5 sec): visual examples}
    \begin{figure}
        \includegraphics[width=9cm]{img/figure5_midterm_visual__fseg.png}
        \caption{Variations of \model{S}{S} vs. optical flow.
                 First row: last input and GT. Other rows: prediction overlays on true future RGB frames}
    \end{figure}
\end{frame}

\begin{frame}{Mid-term future ($\leq$ 0.5 sec): visual examples at $t+3$}
    \begin{figure}
        \includegraphics[width=5.5cm]{img/mid-term-batch-424-spred_4.png}
        \caption{{\small GT segmentation at $t+3$}}
    \end{figure}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[width=5.5cm]{img/mid-term-ARft-106-spred_5.png}
        \caption{{\small AR-ft predicted segmentation at $t+3$}}
    \end{figure}
\end{frame}

\begin{frame}{Mid-term future ($\leq$ 0.5 sec): visual examples at $t+9$}
    \begin{figure}
        \includegraphics[width=5.5cm]{img/mid-term-AR-106-gt_.png}
        \caption{GT segmentation at $t+9$}
    \end{figure}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[width=5.5cm]{img/mid-term-ARft-106-spred_7.png}
        \caption{AR-ft predicted segmentation at $t+9$}
    \end{figure}
\end{frame}


\begin{frame}{Long-term future ($\leq$ 10 sec)}
    \begin{figure}
        \includegraphics[width=7cm]{img/fig7_longterm__fseg.pdf}
        \caption{AR and AR-finetune models vs. last-input-copy baseline: IoU SEG of long-term predictions.}
    \end{figure}
\end{frame}

\begin{frame}{Long-term future ($\leq$ 10 sec): visual examples}
    \begin{figure}
        \includegraphics[width=11cm]{img/figure6_longterm_visual__fseg.png}
        \caption{Top: GT segmentations at t (last input frame), t+1s, t+4s, t+7s, t+10s.
                 Bottom: Segmentation predictions of \model{S}{S}, AR-fine-tune}
    \end{figure}
    \begin{itemize}
        \item No adversarial loss \\
              $\rightarrow$ unrealistic "average" images, not useful
        \item Long-term prediction with adversarial loss not visualised in paper
    \end{itemize}
\end{frame}


\section{Conclusion}

\begin{frame}{Summary}
    \begin{itemize}
        \item Using segmentations directly to predict future segmentations \\
        \item No use for raw RGB frame inputs or outputs
        \item GT video segmentations auto-generatable from RGB frames ($\rightarrow$ Dilation10) \\
              $\rightarrow$ No need for existing annotations
        \item Multi-scale architecture benefits
        \item Autoregressive architecture for future prediction
        \item Choosing the right loss very important (GDL, GAN)
        \item Discussed models perform well for $\sim$ +0.5 s.
    \end{itemize}
\end{frame}

\begin{frame}{Criticism}
    \begin{itemize}
        \item Over-stated claim: "Without requiring temporally dense video annotation"
        \item No evaluation of GAN architecture for long-term prediction
        \item No discussion of non-trivial trajectories
        \item Hard to evaluate visual examples and model architecture details
              ($\rightarrow$ supplementary material?)
    \end{itemize}
\end{frame}

\begin{frame}{Outlook}
    \begin{itemize}
        \item GAN on long-term predictions
        \item VAE, LSTM architectures
        \item Dilated convolutions for future frame prediction network
        \item Deriving actual decisions from predictions?
        \item Real-time stream predictions
    \end{itemize}
\end{frame}

\begin{frame}{References}
    \begin{itemize}
        \item (main ref.) \url{https://arxiv.org/abs/1703.07684} \\ \small{Predicting Deeper into the Future of Semantic Segmentation}
        \item (04) \url{https://arxiv.org/abs/1604.01685} \\ \small{The Cityscapes Dataset for Semantic Urban Scene Understanding}
        \item (23) \url{https://arxiv.org/abs/1611.08408} \\ \small{Semantic Segmentation using Adversarial Networks}
        \item (25) \url{https://arxiv.org/abs/1511.05440} \\ \small{Deep multi-scale video prediction beyond mean square error}
        \item (40) \url{https://arxiv.org/abs/1511.07122} \\ \small{Multi-Scale Context Aggregation by Dilated Convolutions}
        \item \tiny{Image on slide "Problem: Predicting future video frames": \url{http://shyrobotics.com/2012/05/14/la-premiere-voiture-sans-pilote-immatriculee-est-une-google-car-cest-au-nevada/}}
        \item \tiny{Image on slide "Dilation10 network (Yu, Koltun 2015)": \url{https://www.cs.toronto.edu/~frossard/post/vgg16/}}
    \end{itemize}
\end{frame}


\end{document}
